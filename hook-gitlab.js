// https://docmiao.com/community/tutorials/how-to-use-node-js-and-github-webhooks-to-keep-remote-projects-in-sync
const secret = "123456";

const http = require('http');
const crypto = require('crypto');
const shell = require('shelljs');
var qs = require('querystring');

// 统一返回状态码及文本信息
function resText(res, args) {
    res.writeHead(args.stateCode, { 'Content-Type': 'text/plain; charset=utf-8' });
    if (args.stateCode !== 200) console.warn(args.msg);
    res.end(args.msg);
}

// 检查 token
function checkToken(header) {
    return header['x-gitlab-token'] === secret
}

// 执行脚本
function execJob(resData, currentRef, callback) {
    const jobFullName = resData.project.path_with_namespace.replace('/', '-').toLowerCase();

    const shellFile = `/home/webhook/${jobFullName}.sh`;
    console.log('start shell exec: ' + shellFile);
    console.log('trigger by ref: ' + currentRef);

    if (shell.exec(shellFile).code !== 0) {
        console.error('exec shell fail');
        callback(false);
    } else {
        console.log('exec shell success');
        callback(true);
    }
}

http.createServer(function (req, res) {
    let body = '';

    const queryStr = req.url.includes('?') ? req.url.split('?')[1] : '';
    const queryObj = qs.parse(queryStr);
    // 指定分支名
    const requireBranch = queryObj.branch || '';
    // 指定(任意)tag
    const requireTag = queryObj.tag || false;

    req.on('data', function (chunk) {
        body += chunk;
    }).on('end', () => {
        try {
            body = JSON.parse(body)
        } catch (e) {
            resText(res, {
                stateCode: 400,
                msg: 'Bad request.'
            });
            return;
        }

        const obj = body;
        const currentRef = obj.ref; // 如: "refs/heads/master", "refs/tags/v1.0.3", 最后一段即分支名

        if (!checkToken(req.headers)) {
            resText(res, {
                stateCode: 400,
                msg: 'Token 不匹配, 终止操作'
            });
            return;
        } else if (requireBranch) {
            if (currentRef !== `refs/heads/${requireBranch}`) {
                resText(res, {
                    stateCode: 400,
                    msg: '分支不匹配, 终止操作: ' + currentRef
                });
                return;
            }
        } else if (requireTag) {
            if (currentRef.indexOf('refs/tags/') !== 0) {
                resText(res, {
                    stateCode: 400,
                    msg: '未检测到 Tag, 终止操作: ' + currentRef
                });
                return;
            }
        }

        execJob(obj, currentRef, (isSuccess) => {
            if (isSuccess) {
                resText(res, {
                    stateCode: 200,
                    msg: 'success'
                });
            } else {
                resText(res, {
                    stateCode: 400,
                    msg: 'exec job fail'
                });
            }
        });
    });
}).listen(18765);
