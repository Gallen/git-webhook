// https://docmiao.com/community/tutorials/how-to-use-node-js-and-github-webhooks-to-keep-remote-projects-in-sync
const secret = "123456";

const http = require('http');
const crypto = require('crypto');
const shell = require('shelljs');
var qs = require('querystring');

http.createServer(function (req, res) {
    let body = [];
    let chunkStr = '';

    const queryStr = req.url.includes('?') ? req.url.split('?')[1] : '';
    const queryObj = qs.parse(queryStr);
    const requireBranch = queryObj.branch || '';
    const requireTag = queryObj.tag || false;

    req.on('data', function (chunk) {
        body.push(chunk);
        chunkStr = chunk.toString();

    }).on('end', () => {
        body = Buffer.concat(body).toString();

        const payloadObj = qs.parse(body);
        const obj = JSON.parse(payloadObj.payload);
        const currentRef = obj.ref; // 如: "refs/heads/master", "refs/tags/v1.0.3", 最后一段即分支名

        if (requireBranch) {
            if (currentRef !== `refs/heads/${requireBranch}`) {
                console.warn('分支不匹配, 终止操作: ' + currentRef);
                return;
            }
        } else if (requireTag) {
            if (currentRef.indexOf('refs/tags/') !== 0) {
                console.warn('未检测到 Tag, 终止操作: ' + currentRef);
                return;
            }
        }

        const fullName = obj.repository.full_name.replace('/', '-');


        let sig = "sha1=" + crypto.createHmac('sha1', secret).update(chunkStr).digest('hex');

        if (req.headers['x-hub-signature'] == sig) {
            const shellFile = `/home/webhook/${fullName}.sh`;
            console.log('start shell exec: ' + shellFile);
            console.log('trigger by ref: ' + currentRef);

            // 执行脚本
            if (shell.exec(shellFile).code !== 0) {
                console.error('exec shell fail');
            } else {
                console.log('exec shell success');
            }
        } else {
            console.error('signature error');
        }

    });

    res.end();
}).listen(18765);
