# git-webhook

nodejs webhook for github/gitlab，自动触发服务器上的脚本运行

## Usage

```sh
# default use github api
node hook.js

# use gitlab api
node hook-gitlab.js
```

## Configure webhook 配置 webhook

in Github/Gitlab Webhooks config page:

在 Github/Gitlab Webhooks 配置页:

- Payload URL: `http://host.com:18765/`
- Payload URL(appoint branch): `http://host.com:18765/?branch=gh-pages`
- Payload URL(require tag): `http://host.com:18765/?tag=any`
- Secret(change in hook.js): `123456`

## Configure shell 配置脚本

in Linux:

在 Linux 服务器上:

```sh
$ touch /home/webhook/{username}-{projectname}.sh
$ chmod +x /home/webhook/{username}-{projectname}.sh
```

It exec `/home/webhook/{username}-{projectname}.sh` automatically.

本服务会(在接收到 webhook 时)自动运行`/home/webhook/{username}-{projectname}.sh`

(e.g. `/home/webhook/hglcloud-github-webhook.sh`)
